import { LiveChatPage } from './app.po';

describe('live-chat App', () => {
  let page: LiveChatPage;

  beforeEach(() => {
    page = new LiveChatPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
