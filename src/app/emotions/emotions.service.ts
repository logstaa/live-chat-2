import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService } from 'app/auth/auth.service';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/Rx';

@Injectable()
export class EmotionService {
  private emotionsPath: string = '/emotions/';
  private emotionsStart: Subject<any>;

  constructor(private afd: AngularFireDatabase, private auth: AuthService) { }

  add(emotion: string, channel: string): firebase.Promise<any> {
    let updateData = {};
    let newEmotionKey = firebase.database().ref('emotions').push().key;
    let now = firebase.database.ServerValue.TIMESTAMP;
    let uid = this.auth.user.uid;

    updateData['emotions/' + newEmotionKey] = {
      uid: uid,
      createdOn: now,
      emotion: emotion,
      channel: channel
    };

    updateData['lastEmotion/' + uid] = now;

    return this.afd.object('/').update(updateData);
  }

  getRecent(channel: string) {
    return this.afd.list(this.emotionsPath, {
      query: {
        limitToLast: 100,
        orderByChild: 'channel',
        equalTo: channel
      }
    })
  }

  getLast() {
    let uid = this.auth.user.uid;

    return this.afd.object('lastEmotion/' + uid)
  }
}
