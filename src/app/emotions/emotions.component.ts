import { AuthService } from '../auth/auth.service';
import { EmotionService } from './emotions.service';

import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-emotions',
  templateUrl: './emotions.component.html',
  styleUrls: ['./emotions.component.scss']
})
export class EmotionsComponent implements OnInit {
  @Input() channelName: string;
  @Input() userCount: number;
  recentEmotions: any[];
  emotionSizes: any = {};
  cursorType: string = 'pointer';
  lastEmotion: number;
  emotions: string[] = ['happy', 'laughing', 'angry', 'sad', 'surprised', 'confused'];
  // doFadeUp: object = {};
  emotionSubscription: Subscription;
  lastEmotionSubscription: Subscription;

  constructor(private emotionService: EmotionService, private auth: AuthService) { }

  ngOnInit() {
    let uid = this.auth.user.uid;

    this.lastEmotionSubscription = this.emotionService.getLast()
      .subscribe(timeStamp => {
        this.lastEmotion = timeStamp.$value;
      })

    this.emotionSubscription = this.emotionService.getRecent(this.channelName)
      .subscribe(emotions => {
        this.recentEmotions = emotions;
      });

    setInterval(() => {
      let emotionSizes = _.chain(this.recentEmotions)
        .filter((emotion) => {
          return emotion.createdOn > (new Date().getTime() - 5000);
        })
        .countBy('emotion')
        .mapValues((emotionCount: number) => {
          let scale = 0.2 + (emotionCount * (1 / this.userCount));
          scale = _.clamp(scale, 0.2, 1.2);

          return {
            transform: 'scale(' + scale + ')',
            zIndex: (emotionCount + 1).toString()
          }
        })
        .value();

      _.forEach(this.emotions, emotion => {
        if (!emotionSizes[emotion]) {
          emotionSizes[emotion] = {
            transform: 'scale(0.2)',
            zIndex: '1'
          }
        }
      })

      this.emotionSizes = emotionSizes;
      this.cursorType = this.isRateLimited() ? 'not-allowed' : 'pointer'

    }, 500)

  }

  addEmotion(emotion) {
    if (this.isRateLimited()) {
      return
    }

    this.emotionService.add(emotion, this.channelName)
    // .then(() => {
    //   this.doFadeUp[emotion] = true;

    //   setTimeout(() => {
    //     this.doFadeUp[emotion] = false;
    //   }, 2000)
    // })
  }

  private isRateLimited() {
    return this.lastEmotion && this.lastEmotion > (new Date().getTime() - (5 * 1000));
  }

  ngOnDestroy() {
    this.emotionSubscription.unsubscribe();
    this.lastEmotionSubscription.unsubscribe();
  }

}
