import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

@Injectable()
export class ChannelGuard implements CanActivate {
  constructor(private router: Router) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let forbiddenChars = '.$[]#/ ';

    let bad = _.some(next.params.name, (char) => {
      return _.includes(forbiddenChars, char);
    })

    if(bad) {
      this.router.navigate(['/'])
      return false;
    } else {
      return true;
    }

  }
}
