import { AuthService } from './auth/auth.service';

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  signedIn: boolean = false;

  constructor(auth: AuthService) {
    if (auth.authenticated) {
      return;
    }

    auth.signInAnonymously()
      .then(() => {
        this.signedIn = true;
      })
  }

}
