import { ChannelGuard } from './channel.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { ChannelService } from './channel/channel.service';
import { ChannelComponent } from './channel/channel.component';
import { EmotionService } from './emotions/emotions.service';
import { EmotionsComponent } from './emotions/emotions.component';
import { AppRoutingModule } from './routing.module';
import { AuthModule } from "app/auth/auth.module";
import {FormsModule} from '@angular/forms';
import { DisqusModule } from "ngx-disqus";
import { AbsorbExplanationDialog } from './channel/absorb-explanation.component';
import { LandingComponent } from './landing/landing.component'

@NgModule({
  declarations: [
    AppComponent,
    EmotionsComponent,
    AbsorbExplanationDialog,
    ChannelComponent,
    LandingComponent
  ],
  entryComponents: [
    AbsorbExplanationDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    AuthModule,
    AppRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
    DisqusModule.forRoot('livechat-1')
  ],
  providers: [
    EmotionService,
    ChannelService,
    ChannelGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
