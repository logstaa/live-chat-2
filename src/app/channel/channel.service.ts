import { AuthService } from '../auth/auth.service';

import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from "angularfire2/database";
import * as _ from 'lodash';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/Rx';

@Injectable()
export class ChannelService {
  private userUpboatsPath: string = '/userUpboats/';
  private messagePath: string = '/messages/';
  private userRef: firebase.database.Reference;

  constructor(private afd: AngularFireDatabase, private auth: AuthService) { }

  getUserUpboats(channelName: string): FirebaseObjectObservable<any> {
    let uid: string = this.auth.user.uid;

    return this.afd.object(this.userUpboatsPath + channelName + '/' + uid);
  }

  updateUserUpboats(channelName: string, messageId: string, value: any) {
    let uid: string = this.auth.user.uid;

    firebase.database().ref(this.userUpboatsPath + channelName + '/' + uid  + '/' + messageId)
      .set(value)
      .then(() => {
        firebase.database().ref('messages')
          .child(messageId)
          .transaction((message) => {
            message.upboats = value ? message.upboats + 1 : message.upboats - 1;

            if(value && message.createdOn > (new Date().getTime() - (15 * 1000))) {
              message.updatedOn = value
            }

            message.updatedBy = uid;

            return message;
          });
      });
  }

  getChannelMessages(channelName: string, messageNum: Subject<any>): FirebaseListObservable<any> {
    return this.afd.list(this.messagePath, {
      query: {
        limitToLast: messageNum,
        orderByChild: 'channel',
        equalTo: channelName
      }
    })
  }

  getConversations(): FirebaseListObservable<any> {
    return this.afd.list(this.messagePath, {
      query: {
        orderByChild: 'replyCount',
        startAt: 1,
        limitToLast: 300
      }
    })
  }

  getUsers(channelName: string): FirebaseListObservable<any> {
    let presenceRef = firebase.database().ref('presence');
    this.userRef = presenceRef.push();

    firebase.database().ref('.info/connected')
      .on('value', (snap) => {
        if (snap.val()) {
          this.userRef.onDisconnect().remove();

          this.userRef.set(channelName);
        }
      });

    return this.afd.list('presence', {
      query: {
        orderByValue: true,
        equalTo: channelName
      }
    })
  }

  getLastMessage() {
    let uid: string = this.auth.user.uid;

    return this.afd.object('last_message/' + uid);
  }

  removeUser() {
    this.userRef.remove();
  }

  getSimilarity(comment1: string, comment2: string): number {
    let vec1 = _.countBy(comment1.split(' '))
    let vec2 = _.countBy(comment2.split(' '));

    let intersection = _.intersection(_.keys(vec1), _.keys(vec2));

    let numerator = _.reduce(intersection, (result, word) => {
      return result + (vec1[word] * vec2[word])
    }, 0);

    let sum1 = _.reduce(_.keys(vec1), (result, word) => {
      return result + Math.pow(vec1[word], 2);
    }, 0)

    let sum2 = _.reduce(_.keys(vec2), (result, word) => {
      return result + Math.pow(vec2[word], 2);
    }, 0);

    let denominator = Math.sqrt(sum1) * Math.sqrt(sum2);

    if (!denominator) {
      return 0.0
    } else {
      return numerator / denominator;
    }
  }

  createComment(channelName, commentContent) {
    let uid: string = this.auth.user.uid;
    let updateData = {};
    let newCommentKey = firebase.database().ref('messages').push().key;
    let now = firebase.database.ServerValue.TIMESTAMP;

    updateData['messages/' + newCommentKey] = {
      uid: uid,
      updatedBy: uid,
      content: commentContent,
      createdOn: now,
      updatedOn: now,
      upboats: 1,
      replyCount: 0,
      channel: channelName
    };

    updateData['swarm/' + newCommentKey + '/' + uid] = commentContent;
    updateData['userUpboats/' + channelName + '/' + uid + '/' + newCommentKey] = now;
    updateData['last_message/' + uid] = now;

    this.afd.object('/').update(updateData);
  }

  addToSwarm(channelName, mostSimilarCommentId, newCommentContent) {
    // let uid: string = Math.random().toString(36).substring(7);
    let uid: string = this.auth.user.uid

    firebase.database().ref('swarm')
      .child(mostSimilarCommentId)
      .once('value')
      .then(snapshot => {
        let updateData = {};
        let now = firebase.database.ServerValue.TIMESTAMP;
        let swarm = snapshot.val();

        if (swarm[uid]) {
          return;
        }

        let swarmComments = _.values(swarm);
        swarmComments.push(newCommentContent);

        let bestMatch = _.maxBy(swarmComments, (swarmComment: string) => {
          let similaritySum = _.reduce(swarmComments, (result: number, otherSwarmComment: string) => {
            return result += this.getSimilarity(swarmComment, otherSwarmComment)
          }, 0)

          return similaritySum / swarmComments.length;
        });

        firebase.database().ref('messages')
          .child(mostSimilarCommentId)
          .transaction((originalComment) => {
            originalComment.content = bestMatch;
            originalComment.updatedOn = now;
            originalComment.upboats++;
            originalComment.updatedBy = uid;

            return originalComment;
          });

        updateData['swarm/' + mostSimilarCommentId + '/' + uid] = newCommentContent;
        updateData['userUpboats/' + channelName + '/' + uid + '/' + mostSimilarCommentId] = now;

        this.afd.object('/').update(updateData);
      })
  }

  incrementReplyCount(commentId) {
    firebase.database().ref('messages')
      .child(commentId)
      .child('replyCount')
      .transaction(function (replyCount) {
        return replyCount + 1;
      })
  }

  getCommentByKey(key: string) {
    return firebase.database().ref(this.messagePath + key)
      .once('value')
      .then(snapshot => {
        let comment = snapshot.val();

        if(comment) {
          comment.$key = snapshot.key;
        }
        
        return comment;
      })
  }
    
}
