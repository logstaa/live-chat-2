import { ChannelService } from './channel.service';
import { AbsorbExplanationDialog } from './absorb-explanation.component';

import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { MdSidenav, MdSnackBar, MdDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Subject, Subscription } from 'rxjs/Rx';
import { ObservableMedia } from '@angular/flex-layout';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})
export class ChannelComponent implements OnInit {
  channelName: string;
  userUpboats: any = {};
  comments: any[] = [];
  initialLoad: boolean = true;
  allConversations: any;
  conversations: any;
  userCommentContent: string = '';
  clickedComment: any;
  sortOptions: string[] = ['Date', 'Comments', 'Likes'];
  startOptions: any[] = [{
    display: '30 Min',
    milliseconds: 30 * 60 * 1000
  }, {
    display: '1 Hour',
    milliseconds: 60 * 60 * 1000
  }, {
    display: '3 Hours',
    milliseconds: 180 * 60 * 1000
  }];
  sortBy: string = 'Date';
  conversationStart: number = this.startOptions[2].milliseconds;
  conversationsSubscription: Subscription;
  lastOldMessageKey: string;
  userCount: number = 0;
  subscriptions: any = {};
  conversationOrdering: string = 'desc';
  lastMessageTimestamp: number = 0;
  messageNum: any = new Subject();
  chatLoading: boolean = false;
  messagesToGet: number = 100;
  showNewMessageBtn: boolean = false;
  @ViewChild('conversationNav') public myNav: MdSidenav;
  @ViewChild('conversationsNav') public conversationsNav: MdSidenav;

  constructor(private route: ActivatedRoute,
    private channelService: ChannelService,
    private router: Router,
    private snackBar: MdSnackBar,
    private dialog: MdDialog,
    private titleService: Title,
    private observableMedia: ObservableMedia) { }

  ngOnInit() {
    this.subscriptions.params = this.route.params
      .subscribe(params => {
        this.titleService.setTitle('#' + params.name);
        this.channelName = params.name.toLowerCase();

        this.subscriptions.queryParams = this.route.queryParams
          .subscribe(queryParams => {
            let key = queryParams['message'];

            if (!key) {
              return;
            }

            this.channelService.getCommentByKey(key)
              .then((comment) => {
                if (comment) {
                  setTimeout(() => {
                    this.clickedComment = comment;
                  });

                  this.myNav.open();
                } else {
                  this.router.navigate(['/hive/' + this.channelName])
                }
              })
          });

        this.subscriptions.userUpboats = this.channelService.getUserUpboats(this.channelName)
          .subscribe((result) => {
            this.userUpboats = result.$value === null ? {} : result;
          });

        this.subscriptions.messages = this.channelService.getChannelMessages(this.channelName, this.messageNum)
          .subscribe(comments => {
            let shouldScrollDown = false;
            let lineDays = {};

            if (comments.length === this.comments.length + 1 || this.initialLoad) {
              if (this.getPixelsUp() < 50) {
                shouldScrollDown = true;
              } else {
                if (!this.lastOldMessageKey) {
                  this.lastOldMessageKey = _.last(this.comments).$key;
                  this.showNewMessageBtn = true;
                }
              }
            }

            let topCommentKey = this.chatLoading ? this.comments[0].$key : null;

            this.comments = _.chain(comments)
              .orderBy('updatedOn')
              .map((comment: any) => {
                let updatedOn = moment(comment.updatedOn);
                let today = moment();
                let yesterday = moment().subtract(1, 'day');
                let formattedDate = updatedOn.format('M-D-YYYY');

                if (!lineDays[formattedDate]) {
                  if (updatedOn.isBefore(today, 'year')) {
                    comment.dayLine = updatedOn.format('MMMM Do YYYY');
                    lineDays[formattedDate] = true;
                  } else if (updatedOn.isBefore(today, 'day')) {
                    comment.dayLine = updatedOn.isBefore(yesterday, 'day') ? updatedOn.format('MMMM Do') : 'Yesterday';
                    lineDays[formattedDate] = true;
                  } else {
                    if (!_.isEmpty(lineDays)) {
                      comment.dayLine = 'Today';
                      lineDays[formattedDate] = true;
                    }
                  }
                }

                if (comment.$key === topCommentKey) {
                  comment.scrollTo = true;
                }

                return comment;
              })
              .value();

            this.initialLoad = false;

            if (shouldScrollDown) {
              setTimeout(() => {
                this.scrollToBottom();
              })
            }

            if (this.chatLoading) {
              setTimeout(() => {
                document.getElementsByClassName('scrollto')[0].scrollIntoView();
              });

              this.chatLoading = false;
            }
          });

        this.messageNum.next(this.messagesToGet);

        this.subscriptions.users = this.channelService.getUsers(this.channelName)
          .subscribe((users) => {
            this.userCount = users.length;
          });

        this.subscriptions.lastMessage = this.channelService.getLastMessage()
          .subscribe((timestamp) => {
            this.lastMessageTimestamp = timestamp.$value;
          });

        if (!this.observableMedia.isActive('xs')) {
          this.openConversations();
        }

      });

  }

  getPixelsUp() {
    return document.getElementById('chat-side').scrollHeight - document.getElementById('chat-side').clientHeight - document.getElementById('chat-side').scrollTop;
  }

  openConversations() {
    this.conversationsNav.open();

    this.subscriptions.conversations = this.channelService.getConversations()
      .subscribe(conversations => {
        this.allConversations = conversations;

        this.getConversations()
      })
  }

  getConversations() {
    this.conversations = _.filter(this.allConversations, (conversation: any) => {
      return conversation.channel === this.channelName && conversation.updatedOn > (new Date().getTime() - this.conversationStart)
    });
  }

  closeConversations() {
    this.conversationsNav.close();

    this.subscriptions.conversations.unsubscribe();
  }

  closeConversation() {
    this.myNav.close();

    this.conversationClosed();
  }

  getSize(message) {
    let x = message.upboats * 10;
    let y = 600 - (600 / Math.pow(2, (x / 300)));

    let size = 100 + y;

    return size + '%';
  }

  getBoatStatus(message) {
    return this.userUpboats[message.$key] ? 'pink' : 'lightgray';
  }

  boat(boatedMessage) {
    let value = this.userUpboats[boatedMessage.$key] ? null : firebase.database.ServerValue.TIMESTAMP;

    this.channelService.updateUserUpboats(this.channelName, boatedMessage.$key, value);
  }

  commentEntered() {
    if (!this.userCommentContent) {
      return;
    }

    let now = new Date().getTime();

    let recentComments = _.filter(this.comments, (comment: any) => {
      return comment.createdOn > (now - (15 * 1000))
    });

    let mostSimilarComment = _.maxBy(recentComments, (comment: any) => {
      return this.channelService.getSimilarity(comment.content, this.userCommentContent)
    });


    if (mostSimilarComment && this.channelService.getSimilarity(mostSimilarComment.content, this.userCommentContent) >= 0.75) {
      this.channelService.addToSwarm(this.channelName, mostSimilarComment.$key, this.userCommentContent);

      let snackBarSub = this.snackBar.open('Comment absorbed!', 'What?', {
        duration: 3000
      }).onAction()
        .subscribe(() => {
          let dialogRef = this.dialog.open(AbsorbExplanationDialog);
          dialogRef.afterClosed().subscribe(result => {
            // this.selectedOption = result;

            snackBarSub.unsubscribe();
          });
        })
    } else {
      if (this.lastMessageTimestamp > now - 3500) {
        this.userCommentContent = '';

        let snackBarSub2 = this.snackBar.open('Wait at least 3 seconds after last message!', '', { duration: 3000 })
        return;
      }

      this.channelService.createComment(this.channelName, this.userCommentContent);
    }

    this.userCommentContent = '';
  }

  scrollToBottom(): void {
    try {
      document.getElementById('chat-side').scrollTop = document.getElementById('chat-side').scrollHeight;
    } catch (err) {
      console.error(err);
    }
  }

  resetChat(): void {
    this.scrollToBottom();
    this.removeNotifications();
  }

  removeNotifications() {
    this.showNewMessageBtn = false;

    setTimeout(() => {
      this.lastOldMessageKey = '';
    }, 5000)
  }

  openConversation(comment) {
    this.router.navigate(['/hive/' + this.channelName], { queryParams: { message: comment.$key } })
  }

  onComment() {
    this.channelService.incrementReplyCount(this.clickedComment.$key);
  }

  conversationClosed() {
    this.clickedComment = null;

    this.router.navigate(['/hive/' + this.channelName]);
  }

  sortChanged() {
    switch (this.sortBy) {
      case 'Date':
        this.conversations = _.orderBy(this.conversations, 'updatedOn', this.conversationOrdering);
        break;
      case 'Comments':
        this.conversations = _.orderBy(this.conversations, 'replyCount', this.conversationOrdering);
        break;
      case 'Likes':
        this.conversations = _.orderBy(this.conversations, 'upboats', this.conversationOrdering);
        break;
    }
  }

  sortOrderingChanged() {
    this.conversationOrdering = this.conversationOrdering === 'desc' ? 'asc' : 'desc';

    this.sortChanged();
  }

  onScroll() {
    if (!document.getElementById('chat-side').scrollTop) {
      this.chatLoading = true;
      this.messagesToGet += 100;

      this.messageNum.next(this.messagesToGet);
    }

    if (this.getPixelsUp() === 0) {
      this.removeNotifications();
    }
  }

  ngOnDestroy() {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    })

    this.channelService.removeUser();
  }
}
