import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'absorb-explanation-dialog',
  templateUrl: 'absorb-explanation-dialog.html',
})
export class AbsorbExplanationDialog {
  constructor(public dialogRef: MdDialogRef<AbsorbExplanationDialog>) {}
}