import { ChannelService } from '../channel/channel.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import * as _ from 'lodash';
import { AngularFireDatabase } from "angularfire2/database";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  channels: any[];
  enteredChannel: string;
  showError: boolean;
  channelCounts: any[] = [];
  messageCounts: any[] = [];

  @ViewChild('enterHive')
  enterHive: ElementRef;

  constructor(private router: Router, private afd: AngularFireDatabase, private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle('Bee Yard');
    this.enterHive.nativeElement.focus();

    this.afd.list('presence')
      .subscribe((users) => {
        this.channelCounts = _.chain(users)
          .countBy('$value')
          .map((count, channel) => {
            return {
              channel: channel,
              count: count
            }
          })
          .orderBy('count', 'desc')
          .value();
      });

    this.afd.list('messages', {
      query: {
        orderByChild: 'updatedOn',
        startAt: new Date().getTime() - (180 * 60 * 1000)
      }
    })
      .subscribe(messages => {
        this.messageCounts = _.chain(messages)
          .countBy('channel')
          .map((count, channel) => {
            return {
              channel: channel,
              count: count
            }
          })
          .orderBy('count', 'desc')
          .value();
      })
  }

  channelEntered() {
    let forbiddenChars = '.$[]#/ ';

    let bad = _.some(this.enteredChannel, (char) => {
      return _.includes(forbiddenChars, char);
    })

    if (bad) {
      this.showError = true;
      return;
    }

    this.goToChannel(this.enteredChannel.toLowerCase());
  }

  goToChannel(channel) {
    this.router.navigate(['/hive/' + channel])
  }

  getErrorState(control) {
    return this.showError;
  }
}
