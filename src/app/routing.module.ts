import { ChannelGuard } from './channel.guard';
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChannelComponent } from "app/channel/channel.component";
import { LandingComponent } from './landing/landing.component'

const routes: Routes = [
  { path: '',  component: LandingComponent },
  { path: 'hive/:name',  component: ChannelComponent, canActivate:[ChannelGuard]}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

