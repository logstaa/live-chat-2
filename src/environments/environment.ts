// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDeQ1YbIoYqjLJARuh8F_lBuK5NDRCUTnU",
    authDomain: "live-chat-ac188.firebaseapp.com",
    databaseURL: "https://live-chat-ac188.firebaseio.com",
    projectId: "live-chat-ac188",
    storageBucket: "live-chat-ac188.appspot.com",
    messagingSenderId: "635652257463"
  }
};
